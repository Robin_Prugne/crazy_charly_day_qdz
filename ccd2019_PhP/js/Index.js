function openPage(pageName, elmnt, color) {
    //cache tout les tabcontents
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    //enlève le background des boutons / tablinks
    tablinks = document.getElementsByClassName("tablink");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].style.backgroundColor = rgb(90,90,90);
    }

    //affiche le block
    document.getElementById(pageName).style.display = "block";

    //change la couleur du bouton
    elmnt.style.backgroundColor = color;
}


document.getElementById("defaultOpen").click();

function setColor(event){
    event.target.classList.toggle("Home");
}

window.addEventListener("load", () => {
    let btn1 = document.querySelector("#accueil");
    let btn2 = document.querySelector("#btn");
    let btn3 = document.querySelector("#login");
    let btn4 = document.querySelector("#enregistrer");
    btn1.addEventListener("click", setColor);
    btn2.addEventListener("click", setColor);
    btn3.addEventListener("click", setColor);
    btn4.addEventListener("click", setColor);
});