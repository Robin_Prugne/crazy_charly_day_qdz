<?php
/**
 * Created by PhpStorm.
 * User: vallera4u
 * Date: 07/02/2019
 * Time: 09:52
 */

namespace ccd2019\vue;


class VueIndex
{

    public function css() {
        $html=<<<END
        
        header {
            color: rgb(95,135,255);
            text-align: center;
            
        }
        
        .titre{
            font-size: 100px;
            background: url(././Images/travail_entete.jpg);
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: cover;
            background-position: center top;
            opacity: 0.60;
        }
        
        .a_propos{
            margin-top: 50px;
            margin-left: 30px;
            font-weight: bold;
            font-size: 50px;
            color: #444;
        }
        
        .paragraphe{
            
        }
        
        .info{
            font-size: 30px;
            margin-top: 30px;
            margin-left: 80px;
            color: black;
        }
        
        body{
            background-color: #888;
        }
                
        footer {
            text-align: center;
            position: fixed;
            bottom: 0;
            width: 100%;
            font-size: 10px;
            background-color: #555;
        }
        
        .container-fluid{
            background-color: #555;
            width: 100%;
        }
        
        .grpBtn {
            display: flex;
            justify-content: center;
            width: 100%;
        }
               
        body, html {
            height: 100%;
            margin: 0;
            font-family: Arial;
        }

        .btn {
            background-color: #555;
            color: white;
            float: left;
            border: none;
            outline: none;
            cursor: pointer;
            padding: 14px 16px;
            font-size: 17px;
            width: 25%;
        }

        .btn:hover {
            background-color: #777;
        }

        .grpBtn {
            color: white;
            display: flex;
            padding-left: 20px;
            padding-right: 20px;
        }


END;
        return $html;
    }

    private function information(){
        $str="Just Job est un site vous permettant de postuler pour un job de manière spontannée.</br>
            L'organisation a pour but de valoriser le travail pour les personnes handicapées ainsi que du covoiturage </br>
            pour que ces mêmes personnes puissent accéder facilement à leur lieu de travail.";
        return $str;
    }

    public function render($num){

        switch ($num) {
            case 1 :
                {
                    $content = $this->information();
                    break;
                }
        }

        $css=$this->css();

        $b1 = "Accueil";
        $b2 = "Transport";
        $b3 = "Proposer un emploi";
        $b4 = "Afficher les emplois";
        $b5 = "Afficher les candidatures";
        $contact = "Vous pouvez nous contacter à notre adresse mail justjob@orga.fr</br>
                    ou bien par téléphone au +33 xx xx xx xx xx";

        $html=<<<END
<!DOCTYPE html>
<head>

    <title>JustJob</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"></head>
    <style>
        $css
    </style>

<header class="page-header">
<div>
    <h1 class="titre">Just Job</h1>
</div>
</header>

<body>
<div class="container-fluid">
    <nav class="grpBtn">
        <a href="./" class="btn btn-primary" id="acceuil">$b1</a>
        <a href="./transport" class="btn btn-primary" id="transport">$b2</a>
        <a href="./proposerEmploi" class="btn btn-primary" id="propEmploi">$b3</a>
        <a href="./afficherEmploi" class="btn btn-primary" id="affichEmploi">$b4</a>
        <a href="./afficherCandidature" class="btn btn-primary" id="affichCandid">$b5</a>
    </nav>
</div>

<div>

     <h1 class="a_propos"> A propos de nous </h1>
     <p class="info">$content</p>

     <h1 class="a_propos"> Nous contacter </h1>
     <p class="info">$contact</p>
  
</div>

</body>

<footer>
    <p><strong> Copyright © 2019 Aubert Tom_Vallera Antonio_Prugne Robin_Da Silva Carmo Alexandre_Matuchet Louis - Crazy Charly Day 2019 - IUT Nancy-Charlemagne - DUT Informatique </strong></p>
</footer>

</html>
END;
        echo $html;
    }

}