<?php
/**
 * Created by PhpStorm.
 * User: vallera4u
 * Date: 07/02/2019
 * Time: 09:52
 */

namespace ccd2019\vue;


class VueOffre
{

    private $tab;
    private $app;
    private $rootUri;

    function __construct($array){
        $this->tab = $array;
        $this->app = \Slim\Slim::getInstance();
        $this->rootUri = $this->app->request->getRootUri();
    }


    public function css() {
        #$bg = "$this->rootUri/Images/travail_entete";
        $html=<<<END
        
        header {
            color: rgb(95,135,255);
            text-align: center;
        }
        
        .page-header{
            padding : 0;
            margin : 0;
            margin-top : -20px;
            border-bottom : none;
        }
        
        .titre{
            font-size: 100px;
            background: url(././Images/travail_entete.jpg);
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: cover;
            background-position: center top;
            opacity: 0.60;
        }
        
        .a_propos{
            margin-top: 50px;
            margin-left: 30px;
            font-weight: bold;
            font-size: 50px;
            color: #444;
        }
        
        .paragraphe{
            
        }
        
        .info{
            font-size: 30px;
            margin-top: 30px;
            margin-left: 80px;
            color: black;
        }
        
        .sous-titre{
            margin-top: 5%;
            margin-left: 45%;
        }
        
        .desc{
            margin-left: 45%;
        }
        
        body{
            background-color: #888;
        }
        
        .main{
            margin-bottom : 4%;
        }
                
        footer {
            text-align: center;
            position: fixed;
            bottom: 0;
            width: 100%;
            font-size: 10px;
            background-color: #555;
        }
        
        .container-fluid{
            background-color: #555;
            width: 100%;
        }
        
        .grpBtn {
            display: flex;
            justify-content: center;
            width: 100%;
        }
               
        body, html {
            height: 100%;
            margin: 0;
            font-family: Arial;
            overflow: auto;
        }

        .btn {
            background-color: #555;
            color: white;
            float: left;
            border: none;
            outline: none;
            cursor: pointer;
            padding: 14px 16px;
            font-size: 17px;
            width: 25%;
        }

        .btn:hover {
            background-color: #777;
        }

        .grpBtn {
            color: white;
            display: flex;
            padding-left: 20px;
            padding-right: 20px;
        }

        .search{
            display: block;
            margin-left : 120px;
            margin-top : 50px;
            margin-bottom : 75px;
        }
        
        .search input[type=text] {
            padding: 5px;
            border: none;
            margin : 15px;
            font-size: 17px;
        }
        
        .art{
            background: #666;
            border-radius: 15px;
            padding: 15px;
            min-height: 200px;
            width : 87%;
            display: inline-block;
            box-sizing: border-box;
            margin-top: 25px;
            margin-left : 100px;
            border: 2px solid black;  
        }
        
        .h2Emploi{
            margin-bottom : 30px;
            color : black;
        }
        
        .bout{
            margin-left:45%;  
        }
        
        .imgEmploi{
           display : inline-block;
           vertical-align : top;    
           height : 150px;
           width : 150px;
           border: 3px solid black;
        }
        
        .droite{
            display : inline-block;
            margin-left : 50px;
            margin-top : -1.5%;
            width : 80%;
        }
        
        .topnav {
          overflow: hidden;
          background-color: #e9e9e9;
          margin-right : 20%;
          margin-bottom : 1%;
        }
        
        .topnav a {
          float: left;
          display: block;
          color: black;
          text-align: center;
          padding: 14px 16px;
          text-decoration: none;
          font-size: 17px;
        }
        
        .topnav a:hover {
          background-color: #ddd;
          color: black;
        }
        
        .topnav a.active {
          background-color: #2196F3;
          color: white;
        }
        
        .topnav .search-container {
          float: left;
        }
        
        .topnav input[type=text] {
          padding: 6px;
          margin-top: 8px;
          font-size: 17px;
          border: none;
        }
        
        .topnav .search-container button {
          float: right;
          padding: 6px 10px;
          margin-top: 8px;
          margin-right: 16px;
          background: #ddd;
          font-size: 17px;
          border: none;
          cursor: pointer;
        }
        
        .topnav .search-container button:hover {
          background: #ccc;
        }
        
        .dropdown{
            margin-top : -5.2%;
            margin-left : 82%;
            width : 35%;
        }
        
        .btn1{
            background-color: #555;
            color: white;
            border: none;
            outline: none;
            cursor: pointer;
            padding: 14px 16px;
            font-size: 17px;
            width: 25%;
            margin-top : 25%;
         }

END;
        return $html;
    }

    private function afficherOffre(){
        $str="<article class=\"search\">
            <div class=\"topnav\">
              <div class=\"search-container\">
                  <input type=\"text\" placeholder=\"Search..\" name=\"search\">
                  <button type=\"submit\"><i class=\"fa fa-search\"></i></button>
              </div>
              
            </div>
            
            <div class=\"dropdown\">
                  <button class=\"btn btn-secondary dropdown-toggle\" type=\"button\" id=\"dropdownMenuButton\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                   Trier par
                  </button>
                  
                  <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuButton\">
                    <a class=\"dropdown-item\" href=\"#\">Pertinence</a>
                    <a class=\"dropdown-item\" href=\"#\">Date de publication</a>
                    <a class=\"dropdown-item\" href=\"#\">Ordre alphabétique</a>
                  </div>
             </div>
        </article>";
        foreach($this->tab as $can) {
            $id =$can['id'];
            $titre=$can['titre'];
            $desc=$can['description'];
            $img=$can['img'];
            $idUser=$can['idUser'];

            $str .= "<article class=\"art\">
                    <img src=\"././Images/$img\" class=\"imgEmploi\"> 
                        
                        <div class=\"droite\">
                            <h2 class=\"h2Emploi\"> <a href='{$this->app->urlFor('afficherOffre',array('id' => $id))}'> $titre </a> </h2>
                            <p> $desc </p>
                        </div>
                    </article>";
        }
        return $str;
    }

    private function afficherOffreParId()
    {
        $id =$this->tab['id'];
        $str = "";
        $titre = $this->tab['titre'];
        $desc = $this->tab['description'];
        $img = $this->tab['img'];
        $idUser = $this->tab['idUser'];


        $str .= "<article class=\"art\">
                <img src=\"$this->rootUri/Images/$img\" class=\"imgEmploi\">

                    <div class=\"droite\">
                        <h2 class=\"h2Emploi\"> $titre </h2>
                        <p> $desc </p>
                        <button type=\"button\" class=\"btn1 btn-primary\"> <a href='{$this->app->urlFor('candidater',array('id' => $id))}'> Envoyer la candidature </a> </button>
                    </div>
                </article>";

        return $str;
    }

    public function formOffre(){
        $app = \Slim\Slim::getInstance();
        $url = $app->urlFor('proposerEmploi');
        $str="<article>
                <form method='post' action='$url' >
                <p class='sous-titre'>titre : </p> <p class='desc'><input type='text' name='titre'></p>
                <p class='sous-titre'>description : </p> <p class='desc'><input type='text' name='description'></p>
                <p class='sous-titre'>img : </p> <p class='desc'><input type='text' name='img'></p>
                
                <button class='bout' type='submit'>Valider</button>
                </form></article>";

        return $str;
    }

    public function render($num){

        switch($num){
            case 1 :
                {
                    $content = $this->afficherOffre();
                    break;
                }

            case 2 :
                {
                    $content = $this->afficherOffreParId();
                    break;
                }
            case 3 :
                {
                    $content = $this->formOffre();
                    break;
                }
        }

        $css=$this->css();

        $b1 = "Accueil";
        $b2 = "Transport";
        $b3 = "Proposer un emploi";
        $b4 = "Afficher les emplois";
        $b5 = "Afficher les candidatures";

        $html=<<<END
<!DOCTYPE html>
<head>

    <title>JustJob</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"></head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    
    <style>
        $css
    </style>

<header class="page-header">
<div>
    <h1 class="titre">Just Job</h1>
</div>
</header>

<body>
<div class="container-fluid">
    <nav class="grpBtn">
        <a href="{$this->app->urlFor('index')}" class="btn btn-primary" id="acceuil">$b1</a>
        <a href="{$this->app->urlFor('transport')}" class="btn btn-primary" id="transport">$b2</a>
        <a href="{$this->app->urlFor('proposerEmploi')}" class="btn btn-primary" id="propEmploi">$b3</a>
        <a href="{$this->app->urlFor('afficherEmploi')}" class="btn btn-primary" id="affichEmploi">$b4</a>
        <a href="{$this->app->urlFor('afficherCandidature')}" class="btn btn-primary" id="affichCandid">$b5</a>
    </nav>
</div>

<div>
    <section class="main">
        
    
        $content;
        
        
    </section>
</div>

</body>

<footer>
    <p><strong> Copyright © 2019 Aubert Tom_Vallera Antonio_Prugne Robin_Da Silva Carmo Alexandre_Matuchet Louis - Crazy Charly Day 2019 - IUT Nancy-Charlemagne - DUT Informatique </strong></p>
</footer>

</html>
END;
        echo $html;
    }

}