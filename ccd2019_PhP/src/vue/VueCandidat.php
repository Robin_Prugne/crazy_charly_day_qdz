<?php


namespace ccd2019\vue;


class VueCandidat{

    private $tab;
    private $app;
    private $rootUri;

    function __construct($array){
        $this->tab = $array;
        $this->app = \Slim\Slim::getInstance();
        $this->rootUri = $this->app->request->getRootUri();
    }

    public function css() {
        #$bg = "$this->rootUri/Images/travail_entete";
        $html=<<<END
         
         body{
            background-color: #777;
            font-family: Arial;
        }
        
        header {
            color: rgb(95,135,255);
            text-align: center;
            
        }
        
        .titre{
            font-size: 100px;
            background: url(././Images/travail_entete.jpg);
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: cover;
            background-position: center top;
            opacity: 0.60;
        }
       
        .grpBtn {
            display: flex;
            justify-content: center;
            width: 100%;
        }
        
        section{
            border-style: solid;
            border-color: black;
            margin-top: 2%;
            margin-left:17%;
            margin-right:5%;
            padding: 2%;
            background-color: #666;
            float:left;
            width:21%;
            height: 600px; 
        }
          
        .candidature{
            border-style: solid;
            border-color: black;
            margin-top: 2%;
            margin-left:7%;
            margin-right:5%;
            padding: 2%;
            background-color: #666;
            float:left;
            width:21%;
        }
             
        .container-fluid{
            background-color: #555;
            width: 100%;
        }
        
        .btn {
            background-color: #555;
            color: white;
            float: left;
            border: none;
            outline: none;
            cursor: pointer;
            padding: 14px 16px;
            font-size: 17px;
            width: 25%;
            font-family: Arial;
            margin: 0, 5px;
        }

        .btn:hover {
            background-color: #777;
        }

        .grpBtn {
            justify-content: center;
            width: 100%;
            color: white;
            display: flex;
            padding-left: 20px;
            padding-right: 20px;
        }
        
         
        footer {
            text-align: center;
            position: fixed;
            bottom: 0;
            width: 100%;
            font-size: 10px;
            background-color: #555;
        }
        
        .bout{
            margin-left:25%;
            margin-top: 5%;  
        }
            
    }
        
        

END;
        return $html;
    }


    private function afficherCandidatures(){
        $str="";
        foreach($this->tab as $can) {
        $nom=$can['nom'];
        $prenom=$can['prenom'];
        $mail=$can['mail'];
        $adresse=$can['adresse'];
        $codePostal=$can['codePostal'];
        $ville=$can['ville'];
        $numTel=$can['numTel'];
        $motiv=$can['motiv'];
        $handicape=$can['handicape'];
        $type=$can['typeTransport'];

            $str .= "<article class='candidature'><strong><h1>Candidature</h1></strong>
                <h2 class='candidatures'>Détails :</h2>
                <p><strong>Nom: </strong>$nom</p>
                <p><strong>Prénom: </strong>$prenom</p>
                <p><strong>mail: </strong>$mail</p>
                <p><strong>Adresse: </strong>$adresse</p>
                <p><strong>Code Postal: </strong>$codePostal</p>
                <p><strong>Ville: </strong>$ville</p>
                <p><strong>Téléphone: </strong>$numTel</p>
                <p><strong>Motivations: </strong>$motiv</p>
                <p><strong>Handicape: </strong>$handicape</p>
                <p><strong>Transport nécessaire: </strong>$type</p>
                </article>";
            }
        return $str;
    }

    /*
     $str .= "<article><strong>Candidature</strong>
                <p class='candidatures'>Détails :</p>
                <p><strong>Nom:</strong>4195</p><br>
                <p><strong>Prénom:</strong>Didier</p><br>
                <p><strong>mail:</strong>DupontDidier@gmail.com</p><br>
                <p><strong>Adresse:</strong>13 rue des bambinos</p><br>
                <p><strong>Code Postal:</strong>54000</p><br>
                <p><strong>Ville:</strong>Nancy</p><br>
                <p><strong>Téléphone:</strong>0378594269</p><br>
                <p><strong>Motivations:</strong>J'aime les pommes</p><br>
                <p><strong>Handicape:</strong>Oui</p><br>
                <p><strong>Type transport nécessaire:</strong>Voiture</p><br>";
     */

    public function render($num){


            switch ($num) {
                case 1 :
                    {
                        $content = $this->afficherFormulaire();
                        break;
                    }
                case 2 :
                    {
                        $content = $this->afficherCandidatures() ;
                        break;
                    }
            }

        $css=$this->css();

        $b1 = "Accueil";
        $b2 = "Transport";
        $b3 = "Proposer un emploi";
        $b4 = "Afficher les emplois";
        $b5 = "Afficher les candidatures";

        $html=<<<END
<!DOCTYPE html>
<head>

    <title>JustJob</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"></head>
    <style>
        $css
    </style>

<header class="page-header">
<div>
    <h1 class="titre">Just Job</h1>
</div>
</header>

<body>
<div class="container-fluid">
    <nav class="grpBtn">
        <a href="{$this->app->urlFor('index')}" class="btn btn-primary" id="acceuil">$b1</a>
        <a href="{$this->app->urlFor('transport')}" class="btn btn-primary" id="transport">$b2</a>
        <a href="{$this->app->urlFor('proposerEmploi')}" class="btn btn-primary" id="propEmploi">$b3</a>
        <a href="{$this->app->urlFor('afficherEmploi')}" class="btn btn-primary" id="affichEmploi">$b4</a>
        <a href="{$this->app->urlFor('afficherCandidature')}" class="btn btn-primary" id="affichCandid">$b5</a>
    </nav>
</div>

<div>
    $content
</div>

</body>

<footer>
    <p><strong> Copyright © 2019 Aubert Tom_Vallera Antonio_Prugne Robin_Da Silva Carmo Alexandre_Matuchet Louis - Crazy Charly Day 2019 - IUT Nancy-Charlemagne - DUT Informatique </strong></p>
</footer>

</html>
END;
        echo $html;
    }


    private function afficherFormulaire(){
        $app =\Slim\Slim::getInstance();
        $url = $app->urlFor('candidater');
        $e = $this->tab;
        $id = $e[0]['id'];
        $str = "<article>


        <form method='post' action='$url' >
        <section>
            <p class='rentre'>Nom : </p> <input type='text' name='nom'>
            <p class='rentre'>Prenom : </p> <input type='text' name='prenom'>
            <p class='rentre'>Mail : </p> <input type='text' name='mail'>
            <p class='rentre'>Adresse : </p> <input type='text' name='adresse'>
            <p class='rentre'>Code Postal : </p> <input type='text' name='codePostal'>
            <p class='rentre'>Ville : </p> <input type='text' name='ville'>
            <p class='rentre'>numTel : </p> <input type='text' name='numTel'>
        </section>
        <section>
		    <p class='rentre'>CV : </p> <input type='text' name='cv'>
            <p class='rentre'>Motivation : </p> <input type='text' name='motiv'>
            <p class='rentre'>Handicape : </p> <input type='text' name='handicape'>
            <p class='rentre'>Transport : </p> <input type='text' name='transport'>

            
            <p class='rentre'>Numero d'offre : $id</p> <input type='hidden' name='idOffre' value=$id>
            <button class='btn btn-success'  type='submit'>Valider</button>

        </section>
        </form>
        </article>";
		return $str;
    }

}


