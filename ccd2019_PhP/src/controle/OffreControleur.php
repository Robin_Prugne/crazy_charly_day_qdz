<?php
/**
 * Created by PhpStorm.
 * User: legit
 * Date: 07/02/2019
 * Time: 16:48
 */

namespace ccd2019\controle;

use \ccd2019\vue\VueOffre;
use \ccd2019\modele\Offre;

class OffreControleur{

    public function afficherOffres(){
        $offres = Offre::all();
        $vue = new VueOffre($offres);
        $vue->render(1);
    }

    public function afficherOffre($id){
        $offre = Offre::where('id','=',$id)->first();
        $vue = new VueOffre($offre);
        $vue -> render(2);
    }

    public function creerOffre(){
        $vue = new VueOffre([]);
        $vue->render(3);
    }

    public function saveOffre() {
        $offre = new Offre();

        $titre=filter_var($_POST['titre'],FILTER_SANITIZE_STRING);
        $offre->titre = $titre;

        $descr=filter_var($_POST['description'],FILTER_SANITIZE_STRING);
        $offre->description = $descr;

        $img=filter_var($_POST['img'],FILTER_SANITIZE_STRING);
        $offre->img = $img;

        $offre->save();
    }
}