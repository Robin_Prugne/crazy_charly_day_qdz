<?php
/**
 * Created by PhpStorm.
 * User: alex_
 * Date: 07/02/2019
 * Time: 14:17
 */

namespace ccd2019\controle;

use ccd2019\modele\Candidat;
use ccd2019\modele\Offre;
use \ccd2019\modele\Categorie ;
use \ccd2019\modele\User ;
use \ccd2019\vue\VueCandidat;

class FormulaireControleur{
    public function nvlCandidat($id){
        $e = Offre::where('id','=',$id)->first();
        $vue = new VueCandidat([$e]);
        $vue->render(1);
    }

    public function storeFormulaire(){
        $formu = new Candidat();

        $formu->id_offre = $_POST["idOffre"];

        $nom = filter_var($_POST["nom"] , FILTER_SANITIZE_STRING);
        $formu->nom = $nom;

        $prenom = filter_var($_POST["prenom"] , FILTER_SANITIZE_STRING);
        $formu->prenom = $prenom;

        $mail = filter_var($_POST["mail"] , FILTER_SANITIZE_STRING);
        $formu->mail = $mail;

        $adresse = filter_var($_POST["adresse"] , FILTER_SANITIZE_STRING);
        $formu->adresse = $adresse;

        $codePostal = filter_var($_POST["codePostal"] , FILTER_SANITIZE_STRING);
        $formu->codePostal = $codePostal;

        $ville = filter_var($_POST["ville"] , FILTER_SANITIZE_STRING);
        $formu->ville = $ville;

        $numTel = filter_var($_POST["numTel"] , FILTER_SANITIZE_STRING);
        $formu->numTel = $numTel;

        $cv = filter_var($_POST["cv"] , FILTER_SANITIZE_STRING);
        $formu->cv = $cv;

        $motiv = filter_var($_POST["motiv"] , FILTER_SANITIZE_STRING);
        $formu->motiv = $motiv;

        $handicape = filter_var($_POST["handicape"] , FILTER_SANITIZE_STRING);
        $formu->handicape = $handicape;

        $transport = filter_var($_POST["transport"] , FILTER_SANITIZE_STRING);
        $formu->typeTransport = $transport;

        $formu->id_offre = $_POST["idOffre"];

        $formu->save();
        $this->afficherCandidature();
    }

    public function afficherCandidature(){
        $cand = Candidat::all();
        $vue = new VueCandidat($cand);
        $vue->render(2);
    }
}