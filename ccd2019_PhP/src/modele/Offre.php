<?php
/**
 * Created by PhpStorm.
 * User: legit
 * Date: 07/02/2019
 * Time: 16:50
 */

namespace ccd2019\modele;

class Offre extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'offre';
    protected $primaryKey = 'id';
    public $timestamps = false ;

}