<?php
/**
 * Created by PhpStorm.
 * User: vallera4u
 * Date: 07/02/2019
 * Time: 09:47
 */

namespace ccd2019\modele ;

class Categorie extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'categorie';
    protected $primaryKey = 'id';
    //reactiver si fonction belongsTo ou hasMany
   // protected $timestamps = false ;

}