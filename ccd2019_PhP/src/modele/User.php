<?php
/**
 * Created by PhpStorm.
 * User: vallera4u
 * Date: 07/02/2019
 * Time: 09:50
 */

namespace ccd2019\modele;


class User extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'user';
    protected $primaryKey = 'id';
    //reactiver si fonction belongsTo ou hasMany
   // protected $timestamps = false ;

}