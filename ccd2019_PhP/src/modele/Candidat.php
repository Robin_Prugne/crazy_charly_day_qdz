<?php

namespace ccd2019\modele ;

class Candidat extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'candidat';
    protected $primaryKey = 'id';
    //reactiver si fonction belongsTo ou hasMany
    public $timestamps = false ;


}