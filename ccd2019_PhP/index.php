<?php
/**
 * Created by PhpStorm.
 * User: vallera4u
 * Date: 07/02/2019
 * Time: 09:44
 */

require_once 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;
use \ccd2019\vue\VueIndex as index;
use \ccd2019\controle\OffreControleur;
use \ccd2019\controle\FormulaireControleur;
use \ccd2019\vue\VueTransport;


$db = new DB();
$conf=parse_ini_file('src/conf/conf.ini');
$db->addConnection( $conf );

$db->setAsGlobal();
$db->bootEloquent();


$app = new \Slim\Slim();

$app->get('/', function(){
   $i = new index();
   $i->render(1);
})->name('index');

$app->get('/proposerEmploi', function(){
    $ctrl = new OffreControleur();
    $ctrl->creerOffre();
})->name('proposerEmploi');

$app->post('/proposerEmploi', function() {
    $ctrl = new OffreControleur();
    $ctrl->saveOffre();
});

$app->get('/afficherEmploi', function(){
    $ctrl = new OffreControleur();
    $ctrl->afficherOffres();
})->name('afficherEmploi');

$app->get('/afficherCandidature', function(){
    $ctrl = new FormulaireControleur();
    $ctrl->afficherCandidature();
})->name("afficherCandidature");

$app->get('/candidater/:id',function ($id){
    $ctrl = new FormulaireControleur();
    $ctrl->nvlCandidat($id);
})-> name('candidater');

$app->post('/candidater/:id',function (){
    $ctrl = new FormulaireControleur();
    $ctrl->storeFormulaire();
});

$app->get('/offre/:id',function($id){
    $ctrl = new OffreControleur();
    $ctrl->afficherOffre($id);
})->name('afficherOffre');

$app->get('/transport',function(){
    $vue = new VueTransport();
    $vue->render();
})->name('transport');

$app->run();